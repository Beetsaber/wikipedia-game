package main

import (
	"fmt"
	"sort"
	"strings"
)

func sortPages(jsonData JSON_Data) JSON_Data {

	// take page with longest Displaytitle to be first page
	var best string
	var bestIndx int
	for i, word := range jsonData.Pages {
        if len(word.Displaytitle) > len(best) {
            best = word.Displaytitle
            bestIndx = i
        }
    }
	firstPage = jsonData.Pages[bestIndx]

	fmt.Println("firstPage.Displaytitle:", firstPage.Displaytitle)
	fmt.Println("pages num:", len(jsonData.Pages))

	sort.Sort(byPageTitleSimilarity(jsonData.Pages))
	jsonData.Pages = jsonData.Pages[:5]

	for _, page := range jsonData.Pages {
		fmt.Println(page.Displaytitle, "  ---->   ", page.Description, "\n")
	}

	return jsonData

}

// SORTING JSON_Data.Pages (by most similar in Displaytitle and Description to firstPage.Displaytitle)
var firstPage Page
type byPageTitleSimilarity []Page

func (s byPageTitleSimilarity) Len() int {
    return len(s)
}
func (s byPageTitleSimilarity) Swap(i, j int) {
    s[i], s[j] = s[j], s[i]
}
func (s byPageTitleSimilarity) Less(i, j int) bool {
	var iClose = 0
	var jClose = 0
	for _, firstS := range strings.Split(firstPage.Displaytitle, " ") {
		if strings.Contains(s[i].Displaytitle, firstS) { iClose++ }
		if strings.Contains(s[i].Description, firstS) { jClose++ }
	}
	if iClose > jClose { return true }
	return false
}