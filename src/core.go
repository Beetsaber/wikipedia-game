package main

import (
	"fmt"
	"math/rand"
	"time"
	"errors"
	"strings"
)

var gameOverTicker *time.Ticker
var failTime time.Time

// nextPuzzle will populate `answers` with correct values; and also place titles as button labels and descriptions as textfieled values
func nextPuzzle() {

	var pages []Page

	// nextPuzzle call after puzzle is solved or on first next puzzle button press
	if out1.Text() == PUZZLE_SOLVED_STR || strings.Contains(statusTE.Text(), PUZZLE_SOLVED_STR) || strings.Contains(statusTE.Text(), PRESS_NEXT_PUZZLE) {
		pages = getNextSetOfQandA().Pages// copy new JSON_Data.Pages taken from getNextSetOfQandA
	} else {// nextPuzzle call without puzzle being solved
		updateStatus(qInx+1, "solve current puzzle first")
		return
	}

	if gameOverTicker == nil {// if it is first run or if time ran out gameOverTicker will be null
		fmt.Println("reset gameOverTicker")
		gameOverTicker = time.NewTicker(time.Second)
		go gameOverWhenTimeRunsOut()
	}
	failTime = time.Now()// moving the goal post

	enableButtons()

	var a []string
	var d []string
	for i := 0; i < len(pages); i++ {
		a = append(a, pages[i].Displaytitle)// form list of titles/answers
		answers[i] = pages[i].Displaytitle// assign titles/answers in order which coresponds to descriptions(outX)
		d = append(d, pages[i].Description)// form list of descriptions
	}
	// assign descriptions to labels
	out1.SetText(d[0])
	out2.SetText(d[1])
	out3.SetText(d[2])
	out4.SetText(d[3])
	out5.SetText(d[4])

	// shuffle descriptions
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(a), func(i, j int) { a[i], a[j] = a[j], a[i] })

	// assign randomly ordered answers to buttons
	btn1.SetText(a[0])
	btn2.SetText(a[1])
	btn3.SetText(a[2])
	btn4.SetText(a[3])
	btn5.SetText(a[4])

	nextQ()

}

func gameOver() {
	if gameOverTicker != nil { gameOverTicker.Stop() }
	fmt.Println("Try to save in file level", level, "score")
	saveHighscore()
	updateLevel(false, 0, false)
	setTopScoreAsStatus()
	// setting Ticker to null will make sure it is restarted when nextPuzzle is called(ie. when next puzzle button is pressed)
	gameOverTicker = nil
}

func gameOverWhenTimeRunsOut() {// goroutine

	var timeBeforeGameover int = 61

	for {

		t := <-gameOverTicker.C

		var diff int = int(t.Sub(failTime).Seconds())
		fmt.Println("time passed:", diff)

		if diff <= timeBeforeGameover {
			updateTime(timeBeforeGameover - diff)
		} else {// time ran out
			gameOver()
			return
		}

	}

}

// gets called only when there was a correct button pressed
func nextQ() {

	// TODO play success sound here
	qInx++

	if qInx >= 5 {
		if gameOverTicker != nil { gameOverTicker.Stop() }
		gameOverTicker = nil
		btn1.SetEnabled(false)
		btn2.SetEnabled(false)
		btn3.SetEnabled(false)
		btn4.SetEnabled(false)
		btn5.SetEnabled(false)
		updateLevel(true, 1, true)
		qInx = -1
		out1.SetText(PUZZLE_SOLVED_STR)// this is required so that nextPuzzle can get new set of questions and aswers
	} else {// correct button pressed but not whole puzzle is solved
		enableButtons()
	}

	out1.SetTextColor(neutralColor)
	out2.SetTextColor(neutralColor)
	out3.SetTextColor(neutralColor)
	out4.SetTextColor(neutralColor)
	out5.SetTextColor(neutralColor)

	if qInx == -1 || qInx == 0 {// qInx == -1 after puzzle is solved(last correct button pressed)
		out1.SetTextColor(highlightColor)
	} else if qInx == 1 {
		out2.SetTextColor(highlightColor)
	} else if qInx == 2 {
		out3.SetTextColor(highlightColor)
	} else if qInx == 3 {
		out4.SetTextColor(highlightColor)
	} else if qInx == 4 {
		out5.SetTextColor(highlightColor)
	} else {
		panic(errors.New("qInx index out of bound"))
	}

	updateStatus(qInx+1, "")
	
}

func enableButtons() {
	if btn1.Enabled() == false { btn1.SetEnabled(true) }
	if btn2.Enabled() == false { btn2.SetEnabled(true) }
	if btn3.Enabled() == false { btn3.SetEnabled(true) }
	if btn4.Enabled() == false { btn4.SetEnabled(true) }
	if btn5.Enabled() == false { btn5.SetEnabled(true) }
}