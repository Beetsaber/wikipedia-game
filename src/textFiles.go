package main

import (
	"strings"
	"strconv"
	"os"
	"encoding/csv"
	"sort"
)

func saveHighscore() {
	var fileName string = "scoreboard.csv"

	var username string = cutoutAfterNewLine(usernameTE.Text())
	if len(username) < 1 || len(username) > 50 {
		username = "Bob"
		println("username length incorrect, using default", username+":"+strconv.Itoa(level))
	}

	var data [][]string// = [][]string{{"uname1", "123"}, {"uname2", "0"}}
	data = readCsvFile(fileName)


	var oldChecksum int
	oldChecksum, err := strconv.Atoi(data[0][1])
	checkError("Checksum NAN", err)
	if calcChecksum(data[1:]) != oldChecksum {
		println("incorrect checksum")
		//println("old checksum:", oldChecksum, "should be:", calcChecksum(data[1:])) // debug
		return
	}


	if !isUsernameInData(username, data) {
		addNewUsername(fileName, username, level)
		updateChecksum(fileName)
		println("returning after adding new username")
		return
	}


	// update existing username-s score

	if !checkIfScoreForUsernameLessThanNewScore(data, username, level) {
		println("score lower than previous. Return")
		return
	}

	file, err := os.OpenFile(fileName, os.O_WRONLY, 0666)
	checkError("Cannot open file " + fileName, err)

	writer := csv.NewWriter(file)

	for _, rowValues := range data {
		if rowValues[0] == username {
			var lvlStr string = strconv.Itoa(level)
			var newRowValues []string = []string{rowValues[0], lvlStr}
			println("adding", newRowValues[0], newRowValues[0])
			err := writer.Write(newRowValues)
			checkError("Cannot write to file", err)
		} else {
			err := writer.Write(rowValues)
			checkError("Cannot write to file", err)
		}
	}

	writer.Flush()
	file.Close()

	updateChecksum(fileName)

}

func updateChecksum(fileName string) {
	// read file again to update checksum to new one after change of username-s level
	var data = readCsvFile(fileName)

	file, err := os.OpenFile(fileName, os.O_WRONLY, 0666)
	checkError("Cannot open file " + fileName, err)

	writer := csv.NewWriter(file)

	var writtenChecksum bool = false
	for _, rowValues := range data {
		if rowValues[0] == "" && len(rowValues) > 1 && len(rowValues[1]) > 0 && writtenChecksum == false {
			var newChecksumStr string = strconv.Itoa(calcChecksum(data[1:]))
			var newRowValues []string = []string{"", newChecksumStr}
			err := writer.Write(newRowValues)
			checkError("Cannot write to file", err)
			writtenChecksum = true
		} else {
			err := writer.Write(rowValues)
			checkError("Cannot write to file", err)
		}
	}

	writer.Flush()
	file.Close()
}

func isUsernameInData(username string, data [][]string) bool {
	for _, rowValues := range data {
		if rowValues[0] == username {
			return true
		}
	}
	return false
}

func addNewUsername(fileName string, username string, score int) {
	file, err := os.OpenFile(fileName, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)// append
	checkError("Cannot open file " + fileName, err)

	writer := csv.NewWriter(file)
	var newRow []string = []string{username, strconv.Itoa(score)}
	err = writer.Write(newRow)
	checkError("Cannot write to file", err)

	writer.Flush()
	file.Close()
}

func checkIfScoreForUsernameLessThanNewScore(data [][]string, username string, newLevel int) bool {
	for _, rowValues := range data {
		if rowValues[0] == username {
			var oldLevel int
			oldLevel, err := strconv.Atoi(rowValues[1])
			checkError("NAN found as score", err)
			if oldLevel < newLevel {
				return true
			}
		}
	}
	return false
}

// FIXME if 2 player have same score username of first one is returned twice
func getTopScores() string {
	var fileName string = "scoreboard.csv"
	var data [][]string = readCsvFile(fileName)


	var oldChecksum int
	oldChecksum, err := strconv.Atoi(data[0][1])
	checkError("Checksum NAN", err)
	if calcChecksum(data[1:]) != oldChecksum {
		println("incorrect checksum")
		return "Unable to read top scores from " + fileName
	}


	scores := make(map[string]int)
	var topScores string

	for _, rowValues := range data {
		var username string = rowValues[0]
		// if not checksum
		if username != "" {
			var lvl int
			lvl, err := strconv.Atoi(rowValues[1])
			checkError("Cannot translate score to int", err)
			scores[username] = lvl
		}
	}

	// To store the keys in slice in sorted order
    var keys []int
    for _, v := range scores {
        keys = append(keys, v)
    }
    sort.Sort(sort.Reverse(sort.IntSlice(keys)))// descending sort.Ints(keys)

    for i, k := range keys {
        if i >= 3 { break }
        key, ok := mapkey(scores, k)
		if !ok {
		  panic("value does not exist in map")
		}
        topScores += clampStr(key, 18) + ":" + strconv.Itoa(k) + "   "
    }
    return topScores
}

func clampStr(s string, l int) string {
	if len(s) > l {
		return s[:l]
	} else {
		return s
	}
}

func mapkey(m map[string]int, value int) (key string, ok bool) {
  for k, v := range m {
    if v == value { 
      key = k
      ok = true
      return
    }
  }
  return
}

func readCsvFile(filePath string) [][]string {
	f, err := os.Open(filePath)
	checkError("Unable to read input file " + filePath, err)

	csvReader := csv.NewReader(f)
	records, err := csvReader.ReadAll()
	checkError("Unable to parse file as CSV for " + filePath, err)

	f.Close()
	return records
}

func checkError(message string, err error) {
	if err != nil {
		panic(err)
	}
}

func calcChecksum(data [][]string) int {
	var checksum int = 0
	for _, row := range data {
		if row[0] == "" {// skip checksum row
			continue
		}
		for _, s := range row {
			for _, c := range s {
				checksum += int(byte(c))
			}
		}
	}
	return checksum
}

func gatherUntilNewLine(data string) string {
	var gathered string = ""
	for i := 0; string(data[i]) != "\r"; i++ {
		gathered += string(data[i])
	}
	return gathered
}

func encrypt(s string) string {
	return s
}
func decrypt(s string) string {
	return s
}

func getLang() string {
	var settingsFilename string = "settings.txt"
	fi, err := os.Stat(settingsFilename)
	if err != nil {
		panic(err)
	}

	f, err := os.OpenFile(settingsFilename, os.O_RDONLY, 0444)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	var data []byte = make([]byte, fi.Size())
	count, err := f.Read(data)
	if err != nil {
		println("Read:", count)
		panic(err)
	}

	var lang string = "en"
	if len(string(data)) > 1 && len(string(data)) <= 10 {
		lang = strings.Replace(  strings.Replace(  strings.Replace(string(data), "\r\n", "", -1)  , "\n", "", -1)  , " ", "", -1)
	}
	return lang
}