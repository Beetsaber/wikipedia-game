package main

import (
	"io/ioutil"
	"math/rand"
	"time"
	"strings"
	"errors"
	"encoding/json"
	"net/http"
	"strconv"
	"fmt"
)

// will return always at least 5 pages
func getPages(useRandomWordFromTextFile bool, customWord string) (JSON_Data, error) {

	var word string
	if useRandomWordFromTextFile {
		word = getRandWordFromFile("concepts.txt")
	} else {
		word = customWord
	}
	fmt.Println("WORD is |" + word + "|")

	var lang = getLang()//"en"
	var url = "https://" + lang + ".wikipedia.org/api/rest_v1/page/related/" + word

	_, err, jsonData := communicate(url)
	if err != nil {
		// return JSON_Data with 5 easy matching pages
		return JSON_Data{[]Page{Page{"1","1","1"},Page{"2","2","2"},Page{"3","3","3"},Page{"4","4","4"},Page{"5","5","5"}}}, err
	}

	// replace description for page which doesnt have one with Page.Extract of that page up to the first .
	for i, page := range jsonData.Pages {
		if page.Description == "" {
			jsonData.Pages[i].Description = page.Extract[:strings.Index(page.Extract, ".")]
		}
	}

	var newJsonData JSON_Data
	// remove pages which fit one of following criteria:
	// with "of" or "disambiguation" in title
	// with "Disambiguation" in description
	// without a too long description (shorter than title+5)
	// without a too short title (shorter than 3)
	for _, page := range jsonData.Pages {
		if strings.Contains(page.Displaytitle, "of") ||
		   strings.Contains(page.Displaytitle, "disambiguation") ||
		   strings.Contains(page.Description, "Disambiguation") ||
		   len(page.Displaytitle) < 3 ||
		   len(page.Description) < len(page.Displaytitle)+5 {
			// pass
		} else {
			// remove mentions of title in description
			page.Description = strings.Replace(page.Description, page.Displaytitle, "________________", -1)
			page.Displaytitle = strings.Replace(page.Displaytitle, "<i>", "", -1)
			page.Displaytitle = strings.Replace(page.Displaytitle, "</i>", "", -1)
			newJsonData.Pages = append(newJsonData.Pages, page)
		}
	}

	if len(jsonData.Pages) < 10 {
		panic(errors.New("getPages.communicate error: not enough pages were obtained from wikipedia("+url+")"))
	}

	// in case too many pages didn't fit previous conditions it is still needed to fill up newJsonData with at least 5 pages
	for i := 0; len(newJsonData.Pages) < 5 && i < len(jsonData.Pages); i++ {
		// if page not already in newJsonData and if page does not have _ in it(because when page is placed in newJsonData words may be substituted with _)
		if !descriptionInPages(jsonData.Pages[i].Description, newJsonData.Pages) && !strings.Contains(jsonData.Pages[i].Description, "________________") {
			newJsonData.Pages = append(newJsonData.Pages, jsonData.Pages[i])
		}
	}

	return newJsonData, nil

}

func descriptionInPages(desc string, pages []Page) bool {
	for _, page := range pages {
		if strings.Contains(page.Description, desc) {
			return true
		}
	}
	return false
}

// returns in string and JSON_Data formats the response when sending GET request defined in url
func communicate(url string) (string, error, JSON_Data) {

	var myClient http.Client
	var req *http.Request
	myClient, req = setUpHeader(url)

	resp, err := myClient.Do(req)// same as "resp, err := myClient.Get(url)" but with header parameter
	check(err)
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	check(err)
	//fmt.Println("\ndebug:", string(body))// debug

	var jsonData JSON_Data
	err = json.Unmarshal(body, &jsonData)
	check(err)

	if resp.StatusCode == 200 {
		return string(body[:]), nil, jsonData
	} else {
		fmt.Println("\ndebug:", string(body))// debug
		return "", errors.New("Response code:" + strconv.Itoa(resp.StatusCode)), jsonData
	}

}

func setUpHeader(url string) (http.Client, *http.Request) {

	req, err := http.NewRequest("GET", url, nil)
	check(err)
	req.Header.Set("accept", "application/json")
	return http.Client{Timeout: 5 * time.Second}, req

}



func getRandWordFromFile(fileName string) string {
	dat, err := ioutil.ReadFile(fileName)
	check(err)
	if len(dat) <= 2 {
		panic(errors.New("Length of words in" + fileName + "file" + string(len(strings.Fields(string(dat)))) + "is too short"))
	}
	return getRandomElement(strings.Fields(string(dat)))
}


func getRandomElement(collection []string) string {
	rand.Seed(time.Now().Unix())
	n := rand.Int() % len(collection)
	return collection[n]
}


func check(e error) {
	if e != nil {
		panic(e)
	}
}

