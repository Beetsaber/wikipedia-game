package main

import (
	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
	"errors"
	"strconv"
	"strings"
)
// build GUI without cmd windows:
// make sure that "rsrc.syso" and "main.exe.manifest" are next to this file when building
//go build -ldflags="-H windowsgui"

// docs: https://godoc.org/github.com/lxn/walk
// https://gowalker.org/github.com/lxn/walk/declarative#PushButton

type Page struct {
	Displaytitle string
	Description string
	Extract string
}
type JSON_Data struct {
	Pages []Page
}

func getNextSetOfQandA() JSON_Data {
	var jsonData JSON_Data
	
	if len(customWordTE.Text()) > 0 &&
	   customWordTE.Text() != DEFAULT_CUSTOM_WORD &&
	   len(cutoutAfterNewLine(customWordTE.Text())) > 0 &&
	   len(cutoutAfterNewLine(customWordTE.Text())) <= 28 {
		jsonData, _ = getPages(false, cutoutAfterNewLine(customWordTE.Text()))
	} else {
		jsonData, _ = getPages(true, "")
	}
	
	if len(jsonData.Pages) < 5 {
		panic(errors.New("number of pages in jsonData after getPages doesn't match number of screen elements"))
	}

	jsonData = sortPages(jsonData)
	if len(jsonData.Pages) != 5 {
		panic(errors.New("number of pages in jsonData after sortPages doesn't match number of screen elements"))
	}

	return jsonData
}

// program keeps in `answers` list of article titles whose order coresponds to order of description(outX list of TextEdit-s)
// button labels with those same article titles are shuffled
// when one button is pressed it checks whether answer to active title(answers[qInx]) equals that buttons text
// if it does qInx is incremented which heighlights next description(outX) and
// makes answers[qInx] equal next title which coresponds to newlly highlighed description for it
var answers []string
var qInx int = -1

var btn1, btn2, btn3, btn4, btn5 *walk.PushButton
var customWordTE, usernameTE, out1, out2, out3, out4, out5 *walk.TextEdit
var highlightColor, neutralColor walk.Color
var statusTE, levelTE, timerTE, mistakesTE *walk.Label

var level int = 0
const PRESS_NEXT_PUZZLE string = "Press Next puzzle"// used to make next puzzle button work first time its pressed
const PUZZLE_SOLVED_STR string = "Puzzle Solved"// used to make next puzzle button work after puzzle is solved
const DEFAULT_CUSTOM_WORD string = "Custom"// used to make get puzzle from text file or custom word if not present

func main() {
	//TODO
	//add setting.txt options to the screen menu(replace 'en' in setting.txt with other 2 letter abbreviated language to use its wikipedia page)
	//make buttons larger
	//recover from 404 by retrying
	//colored text

	answers = []string{"1","2","3","4","5"}// (just in case)should be initialy different from button.Text (ie. != "")

	var r = 240
	var g = 0
	var b = 140
	highlightColor = walk.Color(uint32(r) | uint32(g)<<8 | uint32(b)<<16)
	r = 20
	g = 20
	b = 50
	neutralColor = walk.Color(uint32(r) | uint32(g)<<8 | uint32(b)<<16)

	var buttonsWidget = []Widget{
							PushButton{
								Enabled: false,
								AssignTo: &btn1,
								Text: "",
								OnClicked: clickedAnswer1,
							},
							PushButton{
								Enabled: false,
								AssignTo: &btn2,
								Text: "",
								OnClicked: clickedAnswer2,
							},
							PushButton{
								Enabled: false,
								AssignTo: &btn3,
								Text: "",
								OnClicked: clickedAnswer3,
							},
							PushButton{
								Enabled: false,
								AssignTo: &btn4,
								Text: "",
								OnClicked: clickedAnswer4,
							},
							PushButton{
								Enabled: false,
								AssignTo: &btn5,
								Text: "",
								OnClicked: clickedAnswer5,
							},
						}

	var font = Font{Family:"Arial", PointSize:14}
	
	MainWindow{
		Title:   "Guess the title of wikipedia page",
		Font: font,
		MaxSize: Size{1200, 600},
		Layout: VBox{MarginsZero: false},
		Children: []Widget{
			Composite{
				Layout: HBox{},
				Children: []Widget{
					Label{
						MinSize: Size{600, 70},
						AssignTo: &statusTE,
						Text: PRESS_NEXT_PUZZLE + " to use concepts.txt or enter concept\n" + "Top scores:  " + getTopScores(),
					},
					Label{
						MinSize: Size{200, 40},
						AssignTo: &levelTE,
						Text: "Level: " + strconv.Itoa(level),
					},
					Label{
						MinSize: Size{70, 40},
						AssignTo: &timerTE,
						Text: "Time",
					},
				},
			},
			Composite{
				Layout: HBox{},
				MinSize: Size{700, 70},
				Children: buttonsWidget,
			},
			HSpacer{
				MinSize: Size{700, 10},
			},
			HSplitter{
				MinSize: Size{700, 200},
				Children: []Widget{
					TextEdit{AssignTo: &out1, ReadOnly: true, TextColor: neutralColor},
					TextEdit{AssignTo: &out2, ReadOnly: true, TextColor: neutralColor},
					TextEdit{AssignTo: &out3, ReadOnly: true, TextColor: neutralColor},
					TextEdit{AssignTo: &out4, ReadOnly: true, TextColor: neutralColor},
					TextEdit{AssignTo: &out5, ReadOnly: true, TextColor: neutralColor},
				},
			},
			HSplitter{
				MinSize: Size{700, 70},
				Children: []Widget{
					Label{
						MinSize: Size{70, 40},
						AssignTo: &mistakesTE,
						Text: "",
					},
					Composite{
						Layout: VBox{},
						Children: []Widget{
							TextEdit{
								AssignTo: &customWordTE,
								ReadOnly: false,
								ToolTipText: "Enter custom word to find pages related to that word(concept)",
								Text: DEFAULT_CUSTOM_WORD,
							},
							TextEdit{
								AssignTo: &usernameTE,
								ReadOnly: false,
								ToolTipText: "Enter name to save score with when time runs out",
								Text: "username",
							},
						},
					},
					PushButton{
						Text: "Next puzzle",
						OnClicked: nextPuzzle,
					},
				},
			},
		},
	}.Run()

}

func setTopScoreAsStatus() {
	statusTE.SetText("Top scores:\n" + getTopScores())
}
func updateStatus(questionNum int, message string) {
	
	statusTE.SetText("Question: " + strconv.Itoa(questionNum) + "/5     " + message)

}
func updateLevel(justIncrement bool, newLevel int, updateLabel bool) {

	if justIncrement {
		level += 1
		if updateLabel {
			levelTE.SetText("Level: " + strconv.Itoa(level))
		}
	} else {
		level = newLevel
		if updateLabel {
			levelTE.SetText("Level: " + strconv.Itoa(newLevel))
		}
	}

}
func updateTime(time int) {
	timerTE.SetText("Time remaining: " + strconv.Itoa( time ))
}

func wrongAnswer() {
	if gameOverTicker == nil {
		return// dont count mistakes if time is not running
	}

	const MAX_MISTAKES_ALLOWED int = 3
	if len(mistakesTE.Text()) >= MAX_MISTAKES_ALLOWED {
		println("game over len:", len(mistakesTE.Text()))
		gameOver()
		mistakesTE.SetText("")
	} else {
		mistakesTE.SetText(mistakesTE.Text() + "*")
		println("mistakesTE len:", len(mistakesTE.Text()))
	}
}

func clickedAnswer1() {
	if answers[qInx] == btn1.Text() {
		nextQ()
	} else {
		wrongAnswer()
		btn1.SetEnabled(false)// incorrect answer; guess again
	}
}
func clickedAnswer2() {
	if answers[qInx] == btn2.Text() {
		nextQ()
	} else {
		wrongAnswer()
		btn2.SetEnabled(false)// incorrect answer; guess again
	}

}
func clickedAnswer3() {
	if answers[qInx] == btn3.Text() {
		nextQ()
	} else {
		wrongAnswer()
		btn3.SetEnabled(false)// incorrect answer; guess again
	}
}
func clickedAnswer4() {
	if answers[qInx] == btn4.Text() {
		nextQ()
	} else {
		wrongAnswer()
		btn4.SetEnabled(false)// incorrect answer; guess again
	}
}
func clickedAnswer5() {
	if answers[qInx] == btn5.Text() {
		nextQ()
	} else {
		wrongAnswer()
		btn5.SetEnabled(false)// incorrect answer; guess again
	}
}

// remove string part after and including "\n" and return resulting string
func cutoutAfterNewLine(s string) string {
	if !strings.Contains(s, "\n") { return s }
	var i = strings.Index(s, "\n") - 1
	if i <= 1 { return "" }
	return s[:i]
}
